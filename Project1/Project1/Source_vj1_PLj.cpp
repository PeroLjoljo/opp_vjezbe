#include<iostream>
#include<string>
using namespace std;

class Student {

public:
	string ID, ime, spol;
	int kviz1, kviz2;
	int midtearmscore, finalscore, totalPoints, total;

	void setInfo(string i, string s, string id) {
		ID = id;
		ime = i;
		spol = s;
		midtearmscore = kviz1 = kviz2 = finalscore = total = totalPoints = 0;
	}
	void deleteinfo() {
		ID.clear();
		ime.clear();
		spol.clear();
		midtearmscore = kviz1 = kviz2 = finalscore = total = totalPoints = 0;
	}
	void anzuriraj(int a, int izbor)
	{
		if (izbor == 3)
			midtearmscore = a;
		else if (izbor == 4)
			finalscore = a;
		else if (izbor == 1)
			kviz1 = a;
		else if (izbor == 2)
			kviz2 = a;

		totalPoints = kviz1 + kviz2;

		if (finalscore != 0)
			total = (midtearmscore + finalscore) / 2;
	}
};
int findStudenta(Student *p, string temp)
{
	for (int i = 0; i < 25; i++)
	{
		if (p[i].ID == temp && !(p[i].ID.empty()))
			return i;
	}
	return -1;
}
void Dodaj(Student *p)
{
	string ime, spol, tempID;
	int ind;
	cout << "\n Dodaj:\nunesi novi ID?";
	getline(cin, tempID);
	while (true)
	{
		int i = findStudenta(p, tempID);
		if (i > -1 || p[i].ID.empty())
		{
			cout << "\nID vec postoji ili je prazan \nUnesi novi: ID\n\n0. Izlaz";
			getline(cin, tempID);
		}
		else
			break;
	}
	if (tempID == "0") {}
	else
	{
		for (int i = 0; i < 25; i++)
		{
			if (p[i].ID.empty()) {
				ind = i;
				break;
			}
		}
		cout << "unesi ime:";
		getline(cin, ime);
		cout << "unesi spol:";
		getline(cin, spol);
		p[ind].setInfo(ime, spol, tempID);
	}
}
void print(Student p)
{
	cout << "\nID: " << p.ID << "  Ime: " << p.ime << "\nSpol: " << p.spol << "\n    kviz 1: " << p.kviz1 << "\n    kviz 2:" << p.kviz2
		<< "\n    ukupan broj bodova: " << p.totalPoints << "\n    sredina semestra: " << p.midtearmscore
		<< "\n    kraj semeestra: " << p.finalscore << "\n    zavrsna ocjena: " << p.total << "\n";
}
void printpopis(Student *p)
{
	for (int i = 0; i < 25; i++)
		if (p[i].ID.size() > 0)
			print(p[i]);

}
void ukloni(Student *p)
{
	string temp;
	cout << "\nID studenta za brisanje: ";
	getline(cin, temp);
	int i = findStudenta(p, temp);

	if (i != -1)
		p[i].deleteinfo();
	else
		cout << "\nStudent nije pronaden!";
}

void anz(Student *p)
{
	string temp;
	int a, vrj;
	cout << "\nID studenta za anzuriranje: ";
	getline(cin, temp);
	int i = findStudenta(p, temp);

	if (i > -1)
	{
		while (true)
		{
			cout << "\nIme: " << p[i].ime << "\nSpol: " << p[i].spol << "\n   (1)  kviz 1: " << p[i].kviz1 << "\n   (2)  kviz 2:" << p[i].kviz2
				<< "\n        ukupan broj bodova: " << p[i].totalPoints << "\n   (3)  sredina semestra: " << p[i].midtearmscore
				<< "\n   (4)  kraj semeestra: " << p[i].finalscore << "\n        zavrsna ocjena: " << p[i].total << "\n";

			cout << "\n0. Izlaz\nAnzuriraj polje:";
			cin >> a;
			if (a == 0)
				break;
			cout << "\nnova vrijednost: ";
			cin >> vrj;

			p[i].anzuriraj(vrj, a);
		}
	}
	else
		cout << "\nStudent nije pronaden!";
}
Student minmaxbodovi(Student *p, int c)
{
	int min = 999, max = 0;
	int minInd, maxInd;
	for (int i = 0; i < 25; i++)
	{
		if (!(p[i].ID.empty()) && min > p[i].totalPoints)
		{
			min = p[i].totalPoints;
			minInd = i;
		}
		if (!(p[i].ID.empty()) && max < p[i].totalPoints)
		{
			max = p[i].totalPoints;
			maxInd = i;
		}
	}
	if (c == 6)
		return p[maxInd];
	else
		return p[minInd];

}
void sortiraj(Student *p)
{
	for (int i = 0; i < 25; i++)
	{
		if (!(p[i].ID.empty()))
			for (int j = 0; j < 25; j++)
			{
				if (!(p[i].ID.empty()) && p[i].totalPoints > p[j].totalPoints)
				{
					Student temp = p[i];
					p[i] = p[j];
					p[j] = temp;
				}
			}
	}
}
void pronadji(Student *p)
{
	string temp;
	cout << "\n Pronadi ID: ";
	getline(cin, temp);
	int a = findStudenta(p, temp);
	if (a != -1)
		print(p[a]);
	else
		cout << "\nID nije pronaden";
}
int main()
{
	Student popis[25];
	int c = 99;

	while (c != 0)
	{
		cout << "\n1. Dodaj novi zapis\n" << "2. Ukloni zais\n" << "3. Anzuriraj zapis\n" << "4. Ispis svih\n" <<
			"5. Pronadi studenta po ID-u\n" << "6. Student s najvise bodova\n" << "7. Student s najmanje bodova\n" <<
			"8. Sortiraj po bodovima\n" << "0. Izlaz";
		cin >> c;
		cin.ignore();
		system("CLS");

		if (c == 1)
			Dodaj(popis);
		if (c == 2)
			ukloni(popis);
		if (c == 3)
			anz(popis);
		if (c == 4)
			printpopis(popis);
		if (c == 5)
			pronadji(popis);
		if (c == 6 || c == 7)
			print(minmaxbodovi(popis, c));
		if (c == 8)
			sortiraj(popis);
		if (c == 0)
			break;
	}
}