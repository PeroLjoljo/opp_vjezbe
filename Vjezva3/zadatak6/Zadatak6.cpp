#include<iostream>
#include<string>
#include<vector>

void chekInput(std::vector<std::string>& v)
{
	std::string input;
	unsigned uper = 0, num = 0;
	int N;
	std::cout << "broj stringova?\n";
	std::cin >> N;
	std::cin.ignore();

	for (int i = 0; i < N; i++)
	{
		while (true)
		{
			std::cout << "string brojeva i velikih slova (mora biti kraci od 20)\n";
			std::getline(std::cin, input);

			uper = num = 0;
			for (std::string::iterator it = input.begin(); it < input.end(); it++)
			{
				if (isupper(*it))
					uper++;
				if (isdigit(*it))
					num++;
			}
			if (uper + num == input.size() && input.size() < 20)
				break;

		}
		v.push_back(input);
	}
}
bool chek4num(std::string str)
{
	for (std::string::iterator it = str.begin(); it < str.end(); it++)
		if (isdigit(*it))
			return true;

	return false;
}
void upper_num(std::string& str)
{
	unsigned temp;
	for (unsigned it = 1; it < str.size(); it++)
	{
		temp = it-1;
		if (isupper(str.at(it)) && isdigit(str.at(temp)))
		{
			int n = str.at(temp) - 48;
			str.erase(temp, 1);
			it--;

			for (int i = 1; i < n; i++)
			{
				std::string a;
				a.push_back(str.at(it));
				str.insert(temp, a);
				it++;
				a.empty();
			}
		}
	}
}
void upper_only(std::string& str)
{
	unsigned n = 0, ind;
	for (unsigned it = 0; it < str.size(); it++)
	{
		n = str.size();
		n = 0;
		ind = it;
		while (ind < str.size() && str.at(ind) == str.at(it)) 
		{
			ind++;
			n++;
		}
		if (n > 1)
		{
			std::string a;
			a.push_back(n+48);
			str.insert(ind - 1, a);
			a.empty();
			str.erase(it,n-1);
		}
	}
}
void convert(std::vector<std::string>& v)
{
	for (std::vector<std::string>::iterator it = v.begin(); it < v.end(); it++)
	{
		if (chek4num(*it))
			upper_num(*it);
		else
			upper_only(*it);
	}
}
void printstr(std::vector<std::string>& v)
{
	for (unsigned i = 0; i < v.size(); i++)
	{
		std::cout << v.at(i) << ", ";
	}
}
int main()
{
	
	std::vector<std::string> strings;
	bool flag=false;
	
	chekInput(strings);
	convert(strings);
	std::cout << "\nconverted strings\n\n";
	printstr(strings);
	int a; 
	std::cin >> a;
}