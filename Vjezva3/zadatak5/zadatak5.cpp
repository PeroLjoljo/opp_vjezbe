#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
class Producent {

public:
	void setname(std::string i) { name = i; }
	void setmovie(std::string i) { movie = i; }
	void setyear(int i) { year = i; }
	std::string getname() { return name; }
	std::string getnmovie() { return movie; }
	int getyear() { return year; }
	void setdefoltcnt() { cnt = 0; }
	void setcnt(int i) { cnt = i; }
	int getcnt() { return cnt; }
private: 
	
	int cnt;
	std::string name, movie; 
	int year;

};

void input(std::vector<Producent>& tt)
{
	std::string buffer;
	int year=1;
	unsigned ind = 0;
	Producent v;
	while (year != 0)
	{
		std::cout << "\nunesi ime producenta";
		getline(std::cin, buffer);
		v.setname(buffer);
		std::cout << "\nunesi ime filma";
		getline(std::cin, buffer);
		v.setmovie(buffer);
		std::cout << "\nunesi godinu filma";
		std::cin >> year;
		while (year < 1900 || year > 2019)
		{
			std::cout << "\n nevazeca godina";
			std::cin >> year;
		}
		v.setyear(year);
		v.setdefoltcnt();
		tt.push_back(v);
		std::cout << "\n 1.unesi novog 0.exit";
		std::cin >> year;
		std::cin.ignore();

	}
}

void printtitle(std::vector<Producent>& v, bool f=true)
{
	for (unsigned i = 0; i < v.size(); i++)
	{
			std::cout << "\nime: " << v.at(i).getname() << " film: " << v.at(i).getnmovie() << " godina: " << v.at(i).getyear() << "\n";

	}
}
bool cmopbystring(Producent& a, Producent& b)
{
	return a.getname() < b.getname(); 
}
bool minmax(Producent& a, Producent& b)
{
	return a.getcnt() > b.getcnt();
}
void prebroji(std::vector<Producent>& v)
{
	std::vector<Producent> temp; 
	
	int cnt = 0;
	bool flag=true;
	for (unsigned i = 0; i < v.size(); i++)
	{
		cnt = 0;
		if (!temp.empty())
			for (unsigned k = 0; k < temp.size(); k++)
			{
				if (v.at(i).getname() == temp.at(k).getname())
					flag = false;
				else
					flag = true;
			}
				
		if (flag)
		{
			for (unsigned j = 0; j < v.size(); j++)
			{
				if (v.at(i).getname() == v.at(j).getname())
					cnt++;
			}
			v.at(i).setcnt(cnt);
			temp.push_back(v.at(i));
		}
	}
	std::sort(temp.begin(), temp.end(), minmax);
	for (unsigned k = 0; k < temp.size(); k++)
	{
		std::cout << "autor " << temp.at(k).getname() << " ponavlja se " << temp.at(k).getcnt();
		if(k < temp.size() - 1)
			if (temp.at(k + 1).getcnt() < temp.at(k).getcnt())
				k = temp.size();
	}

}
int main()
{
	std::vector<Producent> titles;

	input(titles);
	//printtitle(titles);
	std::sort(titles.begin(), titles.end(), cmopbystring);
	//printtitle(titles);
	int a;
	prebroji(titles);
	std::cin >> a;
}