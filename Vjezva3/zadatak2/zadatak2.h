#pragma once
#include<iostream>
#include<vector>
#include<algorithm>


std::vector<int> filter(std::vector<int> v1, std::vector<int> v2)
{
	std::sort(v1.begin(), v1.end());
	std::sort(v2.begin(), v2.end());
	std::vector<int> ret;
	for (std::vector<int>::iterator i = v1.begin(); i < v1.end(); i++)
	{
		if (!(std::binary_search(v2.begin(), v2.end(), *i)))
			ret.push_back(*i);
	}
	return ret;
}
