#include"ZooAnimal.h"
#include<iostream>
#include<string>
#include<ctime>

ZooAnimal::ZooAnimal(std::string spc, std::string nme, int birty, int cage, int dmeals, int lifespn)
{
	species = spc; 
	name = nme; 
	birthyear = birty;
	cagenumber = cage;
	dailymeals = dmeals;
	lifespan = lifespn;
	mass = new Yearlyweight [lifespan];
	masscnt = 0; 
}

ZooAnimal::ZooAnimal(const ZooAnimal& copyconst)
{
	species = copyconst.species;
	name = copyconst.name;
	birthyear = copyconst.birthyear;
	cagenumber = copyconst.cagenumber;
	dailymeals = copyconst.getdalymeals;
	lifespan = copyconst.lifespan;
	mass = new Yearlyweight[lifespan];
	for (int i = 0; i < copyconst.masscnt; i++)
	{
		mass[i] = copyconst.mass[i];
	}
	masscnt = copyconst.masscnt;
}

void ZooAnimal::updatemeals(int change)
{
	dailymeals += change;
}

void ZooAnimal::setmassinfo(int y, int m)
{
	time_t tNow = time(0);
	struct tm *timeNow = localtime(&tNow);
	int curentyear = 1900 + timeNow->tm_year;

	bool flag = true; 
	for (int i = 0; i < masscnt; i++)
	{
		if (mass[i].getyear() == y )
		{
			flag = false;
			std::cout << "greska: godina vec postoji" << std::endl;
			i = masscnt;
		}
		if(y == curentyear)
		{
			flag = false;
			std::cout << "greska: tekuca godina" << std::endl;
			i = masscnt;
		}
	}
	if (flag)
	{
		mass[masscnt].setyear(y);
		mass[masscnt].setmass(m);
		addmasscnt();
	}
}

bool ZooAnimal::changeinweight()
{
	time_t tNow = time(0);
	struct tm *timeNow = localtime(&tNow);
	int curentyear = 1900 + timeNow->tm_year;

	double x = 0.0; 
	double z = 0.0;
	int ind;
	for (int i = 0; i < masscnt; i++)
	{
		if (mass[i].getyear() == curentyear)
			ind == i; 
	}
	x = mass[ind].getmass() - mass[ind - 1].getmass(); 
	z = abs(x) > mass[ind].getmass() * 0.1;

	if (x < 0 && abs(x)>z)
	{
		std::cout << "zivotinja je smrsala preko 10% \n";
		dailymeals += 1;
		return true;
	}if (x > 0 && abs(x)>z)
	{
		std::cout << "zivotinja je udebljala preko 10% \n";
		dailymeals -= 1;
		return true;
	}
	return false;
}

void ZooAnimal::printInfo()
{
	std::cout << "vrsta: " << species << "\n  "
		<< " ime: " << name << "\n  "
		<< " godina rodjenja: " << birthyear << "\n  "
		<< " broj kaveza: " << cagenumber << "\n  "
		<< " kolicina dnevnih obroka: " << dailymeals << "\n  "
		<< " ocekivani vijek: " << lifespan << std::endl << "\n  ";

	for (int i = 0; i < masscnt; i++)
	{
		std::cout<< "u godini: " << mass[i].getyear()<<"masa zivotinje: "<< mass[i].getmass() << "\n  ";
	}
			
}

void ZooAnimal::addmasscnt()
{
	masscnt += 1; 
}
int ZooAnimal::getmasscnt()
{
	return masscnt;
}
int ZooAnimal::getyear()
{
	return birthyear;
}
int ZooAnimal::getcagenmbr()
{
	return cagenumber;
}
int ZooAnimal::getdalymeals()
{
	return dailymeals;
}
int ZooAnimal::getlifespan()
{
	return lifespan;
}
std::string ZooAnimal::getspecies()
{
	return species;
}
std::string ZooAnimal::getname()
{
	return name;
}
Yearlyweight* ZooAnimal::getmasinfo()
{
	return mass;
}
void Yearlyweight::setyear(int i)
{
	year = i;
}
void Yearlyweight::setmass(int m)
{
	mass = m; 
}
int Yearlyweight::getyear()
{
	return year;
}
int Yearlyweight::getmass()
{
	return mass;
}
