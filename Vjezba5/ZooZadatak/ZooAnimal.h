#pragma once
#include<iostream>
#include<string>

class Yearlyweight {
	int year;
	int mass;
	 
public: 
	void setyear(int); 
	void setmass(int); 
	int getyear(); 
	int getmass(); 
};

class ZooAnimal {

	std::string species, name;
	int birthyear;
	int cagenumber, dailymeals, lifespan;
	Yearlyweight* mass;
	int masscnt;

public:
	ZooAnimal(std::string spc, std::string name, int birty, int cage, int dmeals, int lifespn);
	ZooAnimal(const ZooAnimal& copyconst);

	void updatemeals(int);
	void setmassinfo(int year, int mass);
	bool changeinweight();
	void printInfo();
	void addmasscnt();
	int getmasscnt();
	int getyear();
	int getcagenmbr();
	int getdalymeals();
	int getlifespan();
	std::string getspecies();
	std::string getname();
	Yearlyweight* getmasinfo();
	
};