#include "Vehicle.h"
using namespace oss;
Land_vehicle::Land_vehicle()
{
	typeofvehicle = "Land";
}
Watercraft::Watercraft()
{
	typeofvehicle = "Water";
}

Aircraft::Aircraft()
{
	typeofvehicle = "Air";
}

Bike::Bike()
{
	passengerscnt = 1;
}


Car::Car()
{
	passengerscnt = 5;
}

Ferry::Ferry(unsigned p, unsigned b, unsigned a)
{
	passengerscnt = p + b + a * 5;
}

Catamaran::Catamaran(unsigned a)
{
	passengerscnt = a;
}

Seaplane::Seaplane(unsigned p)
{
	passengerscnt = p;
}

Counter::Counter()
{
	passngcnt = 0; 
}

void Counter::add(Vehicle* v)
{
	std::cout << v->type() << ", putnika: " << v->passengers() << ". \n";
	passngcnt += v->passengers();
}

unsigned Counter::total()
{
	return passngcnt;
}
