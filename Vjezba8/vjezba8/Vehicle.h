#pragma once
#include<iostream>
#include<string>
using namespace std;
namespace oss
{
	class Vehicle {

	public:
		virtual  ~Vehicle() {};
		virtual string type() { return "NaN"; };
		virtual unsigned passengers() { return 0; };

	};
	class Land_vehicle : virtual public Vehicle {

		string typeofvehicle;
	public:
		Land_vehicle();
		string type() { return typeofvehicle; }

	};
	class Watercraft : virtual public Vehicle {

		string typeofvehicle;
	public:
		Watercraft();
		string type() { return typeofvehicle; }

	};
	class Aircraft : virtual public Vehicle {
		string typeofvehicle;
	public:
		Aircraft();
		string type() { return typeofvehicle; }

	};

	class Bike :virtual public Land_vehicle {
		unsigned passengerscnt;
	public:
		Bike();
		unsigned passengers() { return passengerscnt; }

	};
	class Car :virtual public Land_vehicle {
		unsigned passengerscnt;
	public:
		Car();
		unsigned passengers() { return passengerscnt; }
	};
	class Ferry : virtual public Watercraft {
		unsigned passengerscnt;
	public:
		Ferry(unsigned, unsigned, unsigned);
		unsigned passengers() { return passengerscnt; }
	};
	class Catamaran : virtual public Watercraft {
		unsigned passengerscnt;
	public:
		Catamaran(unsigned);
		unsigned passengers() { return passengerscnt; }

	};
	class Seaplane : virtual public Watercraft, virtual public Aircraft {
		unsigned passengerscnt;
	public:
		Seaplane(unsigned);
		string type() { return "seaplane"; }
		unsigned passengers() { return passengerscnt; }
	};

	class Counter {
		unsigned passngcnt;
	public:
		Counter();
		void add(Vehicle* v);
		unsigned total();
	};
}