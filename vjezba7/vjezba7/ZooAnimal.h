#pragma once
#include<iostream>
#include<string>

class Yearlyweight {
	int year;
	int mass;
	 
public: 
	void setyear(int); 
	void setmass(int); 
	int getyear(); 
	int getmass(); 
};

class ZooAnimal {
protected:
	std::string species, name;
	int birthyear;
	int cagenumber, dailymeals, lifespan;
	Yearlyweight* mass;
	int masscnt;

public:
	ZooAnimal(std::string spc, std::string name, int birty, int cage, int dmeals, int lifespn);
	ZooAnimal(const ZooAnimal& copyconst);

	void updatemeals(int);
	void setmassinfo(int year, int mass);
	bool changeinweight();
	void printInfo()const;
	void addmasscnt();
	int getmasscnt()const;
	int getyear()const;
	int getcagenmbr()const;
	int getdalymeals()const;
	int getlifespan()const;
	std::string getspecies()const;
	std::string getname()const;
	Yearlyweight* getmasinfo();

	virtual void print(std::ostream& os)const {};

	friend std::ostream & operator<<(std::ostream &os, const ZooAnimal& a);

	virtual float addmeals() { return 0.0f; };
};