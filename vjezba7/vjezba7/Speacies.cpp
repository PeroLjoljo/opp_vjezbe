#include "Speacies.h"



void Mammal::print(std::ostream & os) const
{
	os << "\n\nvrsta: sisavac " << species << "\n  "
		<< " ime: " << name << "\n  "
		<< " godina rodjenja: " << birthyear << "\n  "
		<< " broj kaveza: " << cagenumber << "\n  "
		<< " kolicina dnevnih obroka: " << dailymeals << "\n  "
		<< " ocekivani vijek: " << lifespan << "\n  ";

	for (int i = 0; i < masscnt; i++)
	{
		os << "  u godini: " << mass[i].getyear() << "masa zivotinje: " << mass[i].getmass();
	}
	os << "\n   vrijeme gestacije " << gestationPeriod << "\n   prosjecna temp "
		<< averageTemp << "\n   kolicina hrane " << amountoffood * dailymeals << std::endl;

}

Mammal::Mammal() :ZooAnimal("", "", 0000, 00, 0, 0)
{
	gestationPeriod = 0; 
	averageTemp = 0.0f;
	amountoffood = 0.0f;
}

Mammal::Mammal(std::string spc, std::string name, int birty, int cage, int dmeals, int lifespn,
					unsigned gestationPeriod, float averageTemp, float amountoffood)
	: ZooAnimal(spc, name, birty, cage, dmeals, lifespn)
{
	this->gestationPeriod = gestationPeriod; 
	this->averageTemp = averageTemp; 
	this->amountoffood = amountoffood;
}

std::ostream & operator<<(std::ostream & os, const Mammal m)
{
	m.print(os);
	return os; 
}

std::istream & operator >> (std::istream & is, Mammal & m)
{
	std::cout << "\nunesi vrstu sisavca: ";
	is >> m.species;
	std::cout << "\nunesi ime: ";
	is >> m.name;
	std::cout << "\nunesi godinu rodjenja: ";
	is >> m.birthyear;
	std::cout << "\nunesi kavez: ";
	is >> m.cagenumber;
	std::cout << "\nunesi broj obroka : ";
	is >> m.dailymeals;
	std::cout << "\nunesi zivotni vijek: ";
	is >> m.lifespan;
	std::cout << "\nunesi vrijeme gestacije: ";
	is >> m.gestationPeriod;
	std::cout << "\nunesi prosjecnu temp:  ";
	is >> m.averageTemp;
	std::cout << "\nunesi kolicinu po obroku (float): ";
	is >> m.amountoffood;
	return is;
}

std::ostream & operator<<(std::ostream & os, const Bird b)
{
	b.print(os);
	return os;
}

std::istream & operator >> (std::istream & is, Bird & m)
{
	std::cout << "\nunesi vrstu ptice: ";
	is >> m.species;
	std::cout << "\nunesi ime: ";
	is >> m.name;
	std::cout << "\nunesi godinu rodjenja: ";
	is >> m.birthyear;
	std::cout << "\nunesi kavez: ";
	is >> m.cagenumber;
	std::cout << "\nunesi broj obroka : ";
	is >> m.dailymeals;
	std::cout << "\nunesi zivotni vijek: ";
	is >> m.lifespan;
	std::cout << "\nunesi vrijeme inkubacije: ";
	is >> m.incubationtime;
	std::cout << "\nunesi  vrijeme inkubacije:  ";
	is >> m.incubationtime;
	std::cout << "\nunesi kolicinu po obroku (float): ";
	is >> m.amountoffood;
	return is;
}


void Bird::print(std::ostream & os) const
{
	os << "\n\nvrsta: ptica " << species << "\n  "
		<< " ime: " << name << "\n  "
		<< " godina rodjenja: " << birthyear << "\n  "
		<< " broj kaveza: " << cagenumber << "\n  "
		<< " kolicina dnevnih obroka: " << dailymeals << "\n  "
		<< " ocekivani vijek: " << lifespan << std::endl << "\n  ";

	for (int i = 0; i < masscnt; i++)
	{
		os << "  u godini: " << mass[i].getyear() << "masa zivotinje: " << mass[i].getmass();
	}
	os <<  "\n   vrijeme inkubacije " << incubationtime << "\n   prosjecna temp " 
		<< averagetemp << "\n   kolicina hrane " << amountoffood * dailymeals<< std::endl; 
}

Bird::Bird() :ZooAnimal("", "", 0000, 00, 0, 0)
{
	incubationtime = 0;
	averagetemp = 0.0f;
	amountoffood = 0.0f;
}

Bird::Bird(std::string spc, std::string name, int birty, int cage, int dmeals, int lifespn,
			unsigned incubationtime, float averageTemp, float amountoffood)
	: ZooAnimal(spc, name, birty, cage, dmeals, lifespn)
{
	this->incubationtime = incubationtime;
	this->averagetemp = averageTemp;
	this->amountoffood = amountoffood;

}

void Reptile::print(std::ostream & os)const
{
	os << "\n\nvrsta: reptil " << species << "\n  "
		<< " ime: " << name << "\n  "
		<< " godina rodjenja: " << birthyear << "\n  "
		<< " broj kaveza: " << cagenumber << "\n  "
		<< " kolicina dnevnih obroka: " << dailymeals << "\n  "
		<< " ocekivani vijek: " << lifespan << std::endl << "\n  ";

	for (int i = 0; i < masscnt; i++)
	{
		os << "  u godini: " << mass[i].getyear() << "masa zivotinje: " << mass[i].getmass();
	}
	os << "\n   vrijeme inkubacije " << incubationtime << "\n   temp okoline "
		<<  surroudingtemp << "\n   kolicina hrane " << amountoffood * dailymeals << std::endl;

	
}

Reptile::Reptile():ZooAnimal("", "", 0000,00,0,0)
{
	incubationtime = 0; 
	surroudingtemp = 0.0f;
	amountoffood = 0.0f;
}

Reptile::Reptile(std::string spc, std::string name, int birty, int cage, int dmeals, int lifespn,
	unsigned incubationtime, float surroudingtemp, float amountoffood)
	: ZooAnimal(spc, name, birty, cage, dmeals, lifespn)
{
	this->incubationtime = incubationtime;
	this->surroudingtemp = surroudingtemp;
	this->amountoffood = amountoffood;
}

Reptile::~Reptile()
{
}

std::ostream & operator<<(std::ostream & os, const Reptile m)
{
	m.print(os);
	return os;
}

std::istream & operator >> (std::istream & is, Reptile& m)
{
	std::cout << "\nunesi vrstu reptila: ";
	is >> m.species; 
	std::cout << "\nunesi ime: ";
	is >> m.name;
	std::cout << "\nunesi godinu rodjenja: ";
	is >> m.birthyear;
	std::cout << "\nunesi kavez: ";
	is >> m.cagenumber;
	std::cout << "\nunesi broj obroka : ";
	is >> m.dailymeals;
	std::cout << "\nunesi zivotni vijek: ";
	is >> m.lifespan;
	std::cout << "\nunesi vrijeme inkubacije: ";
	is >> m.incubationtime;
	std::cout << "\nunesi  temperaturu okoline:  ";
	is >> m.surroudingtemp;
	std::cout << "\nunesi kolicinu po obroku (float): ";
	is >> m.amountoffood;
	return is;
}




