#pragma once
#include"ZooAnimal.h"

class Mammal: public ZooAnimal{

protected:
	unsigned gestationPeriod; 
	float averageTemp; 
	float amountoffood;
	virtual void print(std::ostream& os)const;
public:
	Mammal();
	Mammal(std::string spc, std::string name, int birty, int cage,
			int dmeals, int lifespn ,unsigned gestationPeriod,float averageTemp,float amountoffood);

	friend std::ostream& operator<<(std::ostream& os, const Mammal m);
	friend std::istream& operator >> (std::istream& is, Mammal& m);
	
	virtual float addmeals() { return amountoffood * dailymeals; }

};

class Bird : public ZooAnimal {

protected:
	unsigned incubationtime; 
	float averagetemp; 
	float amountoffood; 
	virtual void print(std::ostream& os)const;
public:
	Bird();
	Bird(std::string spc, std::string name, int birty, int cage,
		int dmeals, int lifespn, unsigned incubationtime, float averageTemp, float amountoffood);


	friend std::ostream& operator<<(std::ostream& os, const Bird m);
	friend std::istream& operator >> (std::istream& is, Bird& m);
	virtual float addmeals() { return amountoffood * dailymeals; }
};
class Reptile : public ZooAnimal {

protected:
	unsigned incubationtime;
	float surroudingtemp;
	float amountoffood;
	virtual void print(std::ostream& os)const;
public:
	Reptile();
	Reptile(std::string spc, std::string name, int birty, int cage,
		int dmeals, int lifespn, unsigned incubationtime, float surroudingtemp, float amountoffood);
	~Reptile();

	friend std::ostream& operator<<(std::ostream& os, const Reptile m);
	friend std::istream& operator>>(std::istream& is, Reptile& m);
	virtual float addmeals() { return amountoffood * dailymeals; }
};

