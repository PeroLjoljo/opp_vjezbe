#pragma once
#include"Speacies.h"


class Tiger : public Mammal {

public:
	Tiger();
	Tiger(std::string spc, std::string name, int birty, int cage,
		int dmeals, int lifespn, unsigned gestationPeriod, float averageTemp, float amountoffood);
	~Tiger();
	virtual float addmeals() { return amountoffood * dailymeals; }
};

class Owl : public Bird {

public:
	Owl(); 
	Owl(std::string spc, std::string name, int birty, int cage,
		int dmeals, int lifespn, unsigned incubationtime, float averageTemp, float amountoffood);
	~Owl();
	virtual float addmeals() { return amountoffood * dailymeals; }
};

class Crocodile : public Reptile {

public: 
	Crocodile(); 
	Crocodile(std::string spc, std::string name, int birty, int cage,
		int dmeals, int lifespn, unsigned incubationtime, float averageTemp, float amountoffood);
	
	virtual float addmeals() { return amountoffood * dailymeals; }

};
