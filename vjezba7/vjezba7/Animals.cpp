#include"Animals.h"


Tiger::Tiger():Tiger("", "", 0000, 00, 0, 00, 0, 0.0f, 0.0f)
{
}

Tiger::Tiger(std::string spc, std::string name, int birty, int cage, int dmeals,
	int lifespn, unsigned gestationPeriod, float averageTemp, float amountoffood)
	: Mammal(spc, name, birty, cage, dmeals, lifespn, gestationPeriod, averageTemp, amountoffood)
{
}

Tiger::~Tiger()
{
}

Owl::Owl():Bird("","",0000,00,0,00,0,0.0f,0.0f)
{
}

Owl::Owl(std::string spc, std::string name, int birty, int cage, int dmeals, int lifespn,
	unsigned incubationtime, float averageTemp, float amountoffood)
	: Bird(spc, name, birty, cage, dmeals, lifespn, incubationtime, averageTemp, amountoffood)
{
}

Owl::~Owl()
{
}

Crocodile::Crocodile():Reptile()
{
}

Crocodile::Crocodile(std::string spc, std::string name, int birty, int cage, int dmeals, int lifespn,
	unsigned incubationtime, float surroudingtemp, float amountoffood)
	: Reptile(spc, name, birty, cage, dmeals, lifespn, incubationtime, surroudingtemp, amountoffood)
{
}
