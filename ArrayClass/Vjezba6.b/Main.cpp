#include<iostream>
#include"Array.h"
using namespace OPP;
int main()
{
	Array niz; 
	niz.input(2);
	niz.input(3);
	
	std::cout << "pocetni niz \n";
	niz.printarr();

	Array niz2(niz);
	std::cout << "copy const \n";
	niz2.printarr();

	Array niz3; 
	niz3 = niz; 
	std::cout << "operator =  \n";
	niz3.printarr();

	niz = niz2 + niz3;
	std::cout << " niz1 + niz2\n";
	niz.printarr();

	niz.input(9);
	std::cout << "niz1 , niz2 \n";
	niz.printarr();
	niz2.printarr();
	niz = niz - niz2; 
	std::cout << "niz1 - niz2 \n";
	niz.printarr();

	std::cout << "niz1 , niz2 \n";
	niz2.printarr();
	niz3.printarr();
	if (niz2 == niz3) 
		std::cout << "(niz1 == niz2) = true \n";

	std::cout << "niz1 , niz2 \n";
	niz.printarr();
	niz3.printarr();
	if(niz != niz3)
		std::cout << "(niz1 != niz2) = true \n";

	std::cout << "niz \n";
	niz.printarr();
	niz[0] = 123;
	std::cout << "  niz[0] = 123; \n";
	niz.printarr();

}