#include "Array.h"
using namespace OPP;
int Array::instac_counter = 0;

Array::Array()
{
	size = 30; 
	count = 0; 
	arr = new int[size];
	++instac_counter;
}
Array::~Array()
{
	delete [] arr;
}
Array::Array(const Array & copy_arr)
{
	size = copy_arr.count;    //call Array() ???
	count = copy_arr.count;
	arr = new int[copy_arr.count];

	for (int i = 0; i < count; i++)
	{
		arr[i] = copy_arr.arr[i];
	}
	++instac_counter;
} 
void Array::input(int in)
{
	arr[count] = in;
	count += 1;
}
void Array::printarr() const
{
	for (int i = 0; i < count; i++)
	{
		std::cout << arr[i] << " ";
	}
	std::cout << "\n";
}
Array& Array::operator=(const Array & op_arr)
{
	for (int i = 0; i < op_arr.count; i++)
	{
		arr[i] = op_arr.arr[i];
	}
	count = op_arr.count;
	return *this;
}
int & Array::operator[](int i)
{
	return arr[i];
}
int Array::getinstaccounter()
{
	return instac_counter;
}
Array& OPP::operator+(const Array & a1, const Array & a2)
{
	static Array newarr;
	for (int i = 0; i < a1.count + a2.count; i++)
	{
		if (i < a1.count)
			newarr.input(a1.arr[i]);
		else
			newarr.input(a2.arr[i - a1.count]);
	}
	return newarr;
}
Array& OPP::operator-(const Array & a1, const Array & a2)
{
	bool flag; 
	static Array newarr;
	
	for (int i = 0; i < a1.count; i++)
	{
		flag = false;
		for (int j = 0; j < a2.count; j++)
		{
			if (a1.arr[i] == a2.arr[j])
				flag = true; 
		}
		if (!flag)
		{
			newarr.input(a1.arr[i]);
		}
	}
	return newarr;
}
bool OPP::operator==(const Array & a1, const Array & a2)
{
	for (int i = 0; i < a1.size; i++)
	{
		if (i < a1.count && i < a2.count && a1.arr[i] != a2.arr[i])
		{
			return false;
		}
	}
	return true;
}
bool OPP::operator!=(const Array & a1, const Array & a2)
{
	return !(a1 == a2);
}

