#pragma once
#include<iostream>
namespace OPP
{
	class Array {
		int *arr;
		int size;
		int count;
		static int instac_counter;
	public:
		Array();
		~Array();
		Array(const Array& copy_arr);

		void input(int);
		void printarr()const;

		Array& operator=(const Array& op_arr);
		friend Array& operator+(const Array& a1, const Array& a2);
		friend Array& operator-(const Array& a1, const Array& a2);
		friend bool operator==(const Array& a1, const Array& a2);
		friend bool operator!=(const Array& a1, const Array& a2);
		int& operator[](int i);

		static int getinstaccounter();
	};
}