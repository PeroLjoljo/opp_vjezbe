#pragma once
#include<iostream>
#include<ctime>
#include<vector>
#include<math.h>

class Point {
	double x, y, z;
public:
	void setValue(double, double, double);
	double getX() const;
	double getY() const;
	double getZ() const;
};

void setRandomValues(Point& p);

double distance2D(Point& p1, Point& p2);

double distance3D(Point& p1, Point& p2);

