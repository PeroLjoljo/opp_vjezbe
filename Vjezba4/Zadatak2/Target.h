#pragma once
#include"Point.h"


class Target{
	Point position;
	Point secPoint;
	double with, hight;
	bool hit; 
public:
	Target();
	void setvalues();
	Point getposition() const;
	void ishit();
	void setWH(double,double);
	double gethight()const;
	double getwith()const;
	Point getsecondp()const;
};

void printTargets(std::vector<Target> v);
