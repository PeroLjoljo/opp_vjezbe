#include"Wepon.h"
#include"Point.h"
#include"Target.h"

Target::Target()
{
	position.setValue(0.0,0.0,0.0);
	hit = false; 
	with = 0.0;
	hight = 0.0;
}

void Target::setvalues()
{
	setRandomValues(position);
	secPoint.setValue(position.getX()+1.0, position.getY() + with-1.0,position.getZ()+hight);
}

Point Target::getposition()const
{
	return position;
}

void Target::ishit()
{
	hit = true;
}

void Target::setWH(double w,double h)
{
	with = w;
	hight = h;
}

double Target::gethight() const
{
	return with;
}

double Target::getwith() const
{
	return hight;
}
Point Target::getsecondp() const
{
	return secPoint;
}

void printTargets(std::vector<Target> v)
{
	for (unsigned i = 0; i < v.size(); i++)
	{
		std::cout << "meta " << i << " " ;
		std::cout << "x: " << v.at(i).getposition().getX() << "  y: " << v.at(i).getposition().getY() << "  z: " << v.at(i).getposition().getZ() << std::endl;
	}
}