#include"Point.h"
#include"Wepon.h"
void Wepon::setpoint(bool f, double a = 0.0, double b = 0.0, double c = 0.0)
{
	if (f)
		setRandomValues(position);
	else
		position.setValue(a, b, c);
}

Wepon::Wepon()
{
	bullet_number = 0;
	mag_size = 0;
}

void Wepon::reload()
{
	bullet_number = mag_size;
}
void Wepon::shoot()
{
	bullet_number -= 1;
	if (bullet_number < 1)
	{
		reload();
	}
}

Point Wepon::getposition() const
{
	return position;
}

unsigned Wepon::getbullets() const
{
	return bullet_number;
}

unsigned Wepon::getmgsize()
{
	return mag_size;
}
void Wepon::setmgsize(unsigned n)
{
	mag_size = n;
}

bool minmax(Point a, Point b, Point t)
{
	double minx, miny, maxx, maxy;
	if (a.getX() < b.getX())
	{
		minx = a.getX();
		maxx = b.getX();
	}
	else
	{
		minx = b.getX();
		maxx = a.getX();
	}
	if (a.getY() < b.getY())
	{
		miny = a.getY();
		maxy = b.getY();
	}
	else
	{
		miny = b.getY();
		maxy = a.getY();
	}
	if ((minx < t.getX() && t.getX() < maxx) && (miny < t.getY() && t.getY() < maxy))
		return true;
	else
		return false;
}
Point intersection(Point& a, Point& b, Point& c, Point& d)
{
	//pravac AB
	double a1 = b.getY() - a.getY();
	double b1 = a.getX() - b.getX();
	double c1 = a1 * (a.getX()) + b1 * (a.getY());
	// pravac CD
	double a2 = d.getY() - c.getY();
	double b2 = c.getX() - d.getX();
	double c2 = a2 * (c.getX()) + b2 * (c.getY());
	//determinanta 
	double det = a1 * b2 - a2 * b1;

	//tocka sjecista
	double x = (b2*c1 - b1 * c2) / det;
	double y = (a1*c2 - a2 * c1) / det;
	Point ret;
	ret.setValue(x, y, 0.0);

	return ret;
}