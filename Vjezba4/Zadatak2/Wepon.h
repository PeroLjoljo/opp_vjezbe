#pragma once
#include"Point.h"

class Wepon {
	Point position;
	unsigned bullet_number;
	unsigned mag_size;
public:
	Wepon();
	//~Wepon();
	void shoot();
	Point getposition()const;
	unsigned getbullets()const;
	unsigned getmgsize();
	void setmgsize(unsigned);
	void reload();
	void setpoint(bool f, double a, double b, double c);
};

bool minmax(Point a, Point b, Point t);

Point intersection(Point & a, Point & b, Point & c, Point & d);

