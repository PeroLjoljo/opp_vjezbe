#include"Zadatak1.h"

void Point::setValue(double a , double b , double c)
{
	x = a;
	y = b;
	z = c;
}

double Point::getX() const
{
	return x;
}
double Point::getY() const
{
	return y;
}
double Point::getZ() const
{
	return z;
}

void setRandomValues(Point& p)
{
	std::vector<double> v;
	for (int i = 0; i < 3; i++)
	{
		double n = (double)rand() / 100;
		v.push_back(n);
	}
	p.setValue(v.at(0), v.at(1), v.at(2));
	v.clear();
}

double distance2D(Point& p1, Point& p2)
{
	return sqrt(pow((p2.getX() - p1.getX()), 2) + pow((p2.getY() - p1.getY()), 2));
}
double distance3D(Point& p1, Point& p2)
{
	return sqrt(pow((p2.getX() - p1.getX()), 2) + pow((p2.getY() - p1.getY()), 2) + pow((p2.getZ() - p1.getZ()), 2));
}

