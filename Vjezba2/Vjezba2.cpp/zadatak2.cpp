#include <iostream>
#include <cstdlib>
#include <stdlib.h>

using namespace std;

void funkcija2(int* niz1, int n)
{
	int l = 0, r = n - 1;

	while (l < r)
	{
		if (niz1[l] % 2 != 0 && niz1[r] % 2 == 0)
		{
			int temp = niz1[l];
			niz1[l] = niz1[r];
			niz1[r] = temp;
		}
		if (niz1[l] % 2 == 0)
			l++;
		if (niz1[r] % 2 != 0)
			r--;
	}
	cout << "\n";
	for (int i = 0; i < n; i++)
		cout << niz1[i] << " ";
	//7,2,3,4,9,10,11,13,12,27,8 
}
int main()
{
	int niz[] = { 7,2,3,4,9,10,11,13,12,27,8 };
	int cnt = sizeof(niz) / sizeof(int);
	cout << " -- " << cnt << "\n";
	funkcija2(niz, cnt);

}