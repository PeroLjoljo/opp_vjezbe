#pragma once
template<typename T1, typename T2>
class Pair
{
	T1 first;
	T2 second;
public:
	Pair() : first(T1()), second(T2()) {}
	Pair(const T1& t1, const T2& t2) : first(t1), second(t2) {}
	Pair(const Pair<T1, T2>& other) : first(other.first), second(other.second) {}
	bool operator==(const Pair<T1, T2>& other) const
	{
		return first == other.first && second == other.second;
	}
	friend bool operator>(const Pair<T1, T2>& p1, const Pair<T1, T2>& p2)
	{
		return p1.first > p2.first && p1.second > p2.second;
	}
	friend bool operator<(const Pair<T1, T2>& p1, const Pair<T1, T2>& p2)
	{
		return p1.first < p2.first && p1.second < p2.second;
	}
	friend bool operator>=(const Pair<T1, T2>& p1, const Pair<T1, T2>& p2)
	{
		return p1 > p2 || p1 == p2;
	}
	friend bool operator<=(const Pair<T1, T2>& p1, const Pair<T1, T2>& p2)
	{
		return p1 < p2 || p1 == p2;
	}

	friend std::istream& operator >> (std::istream& is, Pair<T1, T2>& p)
	{
		std::cout << "unesi prvi ";
		is >> p.first;
		std::cout << "unesi drugi ";
		is >> p.second;
		return is;
	}
	friend std::ostream& operator <<(std::ostream& os, const Pair<T1, T2>& p)
	{
		os << "\nprvi " << p.first << " drugi " << p.second << std::endl;
		return os;
	}

	 Pair& operator=(const Pair<T1, T2>& p)
	{
		this->first = p.first; 
		this->second = p.second;
		return *this;
	}
};

template<>
class Pair<char*, char*>{
	char * first;
	char * second;
public:

	Pair() : first(new char), second(new char) {}
	Pair(const char t1, const char t2) : first(new char(t1)), second(new char (t2)) {}
	Pair(const Pair<char*, char*>& other) : first(other.first), second(other.second) {}

	//~Pair() { delete first; delete second; }
	friend std::istream& operator >> (std::istream& is, Pair<char* , char*>& p)
	{
		std::cout << "unesi prvi ";
	
		is >> p.first;
		
		std::cout << "unesi drugi ";
		is >> p.second ;
		

		return is;
	}

	friend bool operator<(const Pair<char*, char*>& p1, const Pair<char*, char*>& p2)
	{
		return *p1.first < *p2.first && *p1.second < *p2.second ;
	}
	friend std::ostream& operator <<(std::ostream& os, const Pair<char*, char*>& p)
	{
		os << "\nprvi " << p.first << " drugi " << p.second << std::endl;
		return os;
	}
};