#include<iostream>
#include"Pair.h"
#include<vector>
#include<algorithm>
using namespace std;
bool sortfun(const Pair<char*, char*>& p1, const Pair<char*, char*>& p2)
{
	return p1 < p2;
}
int main()
{
	//using namespace std;
	Pair<char*, char*> p1, p2, p3;	
	vector<Pair<char*, char*> > v;
	
	Pair<int, int> p4(3,4), p5(7,9), p6(9,9);
	
	if (p6 > p4)
		std::cout <<  ">\n";
	if (p6 < p4)
		std::cout << "<\n";

	p5 = p6; 

	cout << p5;

	cin >> p1 >> p2 >> p3;

	v.push_back(p1);
	v.push_back(p2);
	v.push_back(p3);

	sort(v.begin(), v.end(),sortfun);

	vector<Pair<char*, char*> >::iterator it;
	for (it = v.begin(); it != v.end(); ++it)
		cout << *it << endl;
}