#include<iostream>
#include<string>
#include<fstream>	
class Exp {

public:
	std::string err; 
	Exp(const std::string s) : err(s) {}

};

int unos()
{
	int x;
	std::cout << "unesi broj; \n";
	std::cin >> x; 
	if (std::cin.fail())
		throw Exp("greska pri unosu broja cin fail\n");
	return x; 
}
int izboroperatora()
{
	char op; 
	std::cout << "unesi operator + - / * : \n";
	std::cin >> op;
	
	if (std::cin.fail())
		throw Exp("greska pri unosu operatora cin fail\n");

	if (op != '+' && op != '-' && op != '*' && op != '/')
		throw Exp("nevazeci operator\n");
	return op; 
}
double izreacun(int a, int b, int opr)
{
	if (opr == '+')
	{
		return (double)a + b; 
	}
	if (opr == '-')
	{
		return (double)a - b;
	}
	if (opr == '/')
	{
		if (b == 0) throw Exp("nedozvoljeno dijeljenje s nulom");
		return (double)a / b;
	}
	if (opr == '*')
	{
		return (double)a*b;
	}
}
int main()
{
	int a=0, b=0, opr=0; 
	std::ofstream my_file; 
	my_file.open("errors.log", std::ios_base::out | std::ios_base::app);
	try
	{
		while (true)
		{
			a = unos();
			b = unos(); 
			opr = izboroperatora();
			izreacun(a,b,opr);
			std::cout << a; 
		}
		//fout.close();
	}
	catch (Exp T)
	{
		std::cout << "\nerror: " << T.err << std::endl;
		if (my_file.is_open())
		{
			my_file << T.err << std::endl;
		}
		my_file.close();

	}
	system("pause");

}